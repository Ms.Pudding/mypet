package mypet.mypet.web.controller.survey;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SurveyController {
    @GetMapping("/survey")
    public ModelAndView getSurvey(){
        ModelAndView view = new ModelAndView();
        view.setViewName("views/survey/survey_form");
        return view;
    }
}
